package com.practice.myForms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyFormsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyFormsApplication.class, args);
	}

}
